package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.GestorMuebles;
import clases.Mueble;
import clases.Tipo;

class TestMueble {

	private static GestorMuebles gestor;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		gestor = new GestorMuebles();
		gestor.altaTipo(111, "Armarios");
		gestor.altaMueble(1121, "PAX", 256.00, 236, 60, 100, 0, null);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("Fin de los tests.");
	}

	@Test
	void testBuscarMueble() {
		Mueble actual = gestor.buscarMueble("PAX");
		String esperado = "PAX";
		assertEquals(esperado, actual.getNombre());
	}
	
	@Test
	void testBuscarMuebleInexistente() {
		Mueble actual = gestor.buscarMueble("PAXX");
		assertNull(actual);
	}
	
	@Test
	void testAsignarMueble() {
		gestor.asignarTipo(111, "PAX");
		Mueble actual = gestor.buscarMueble("PAX");
		String esperado = "Armarios";
		assertEquals(esperado, actual.getTipoMueble().getNombre());
	}
	
	@Test
	void testAsignarMuebleATipoInexistente() {
		gestor.asignarTipo(111, "PAX");
		Mueble actual = gestor.buscarMueble("PAX");
		String esperado = "Armarios2";
		assertNotEquals(esperado, actual.getTipoMueble().getNombre());
	}
	
	@Test
	void testEliminarMueble() {
		gestor.eliminarMueble("PAX");
		Mueble actual = gestor.buscarMueble("PAX");
		assertNull(actual);
	}
	
	@Test
	void testEliminarMuebleInexistente() {
		gestor.eliminarMueble("PAX2");
		Mueble actual = gestor.buscarMueble("PAX");
		assertNotNull(actual);
	}
	
	@Test
	void testRebajarMuebles() {
		int value = 50;
		gestor.rebajas(value);
		Mueble actual = gestor.buscarMueble("PAX");
		int esperado = value;
		assertEquals(esperado, actual.getDescuento());
	}
	
	@Test
	void testRebajarMueblesNegativo() {
		int value = 50;
		gestor.rebajas(value);
		Mueble actual = gestor.buscarMueble("PAX");
		int esperado = value;
		assertEquals(esperado, actual.getDescuento());
	}
}
