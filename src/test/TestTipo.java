package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.GestorMuebles;
import clases.Tipo;

class TestTipo {
	
	private static GestorMuebles gestor;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		gestor = new GestorMuebles();
		gestor.altaTipo(111, "Armarios");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}
	@Test
	void testBuscarTipo() {
		Tipo actual = gestor.buscarTipo(111);
		String esperado = "Armarios";
		assertEquals(esperado, actual.getNombre());
	}
	@Test
	void testBuscarMTipoInexistente() {
		Tipo actual = gestor.buscarTipo(1112);
		assertNull(actual);
	}
	
}
