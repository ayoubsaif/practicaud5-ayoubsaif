package programa;

import clases.GestorMuebles;

public class Programa {

	public static void main(String[] args) {
		//1
		GestorMuebles gestor = new GestorMuebles();
		//2
		System.out.println("\n1. Alta Tipos de Mueble (CATEGORIAS:)");
		gestor.altaTipo(111, "Armarios");
		gestor.altaTipo(112, "Camas");
		gestor.altaTipo(113, "Sofas");
		gestor.altaTipo(114, "Silla");
		//3
		System.out.println("\n2. Listar tipos de Mueble");
		gestor.listarTipos();
		//4
		System.out.println("\n3. Buscar Muebles por tipo");
		System.out.println(gestor.buscarTipo(111));
		System.out.println(gestor.buscarTipo(115));
		//5
		System.out.println("\n4. Alta de Muebles:");
		gestor.altaMueble(1121, "PAX", 256.00, 236, 60, 100, 0, null);
		gestor.altaMueble(1122, "PLATSA", 196.00, 236, 60, 100, 0, null);
		gestor.altaMueble(1123, "BRIMNES", 185.00, 236, 60, 100, 50, null);
		gestor.altaMueble(1124, "STRANDMON", 199.00, 101, 82, 96, 50, null);
		gestor.altaMueble(1125, "FRIHETEN", 399.00, 66, 140, 151, 0, null);
		//6
		System.out.println("\n5. Asignar Muebles:");
		gestor.asignarTipo(111, "PAX");
		gestor.asignarTipo(111, "PLATSA");
		gestor.asignarTipo(112, "BRIMNES");
		gestor.asignarTipo(113, "STRANDMON");
		gestor.asignarTipo(114, "FRIHETEN");
		//7
		System.out.println("\n6. Listar Muebles por Nombre de Tipo:");
		gestor.listarMueblesTipo("Armarios");
		//8
		System.out.println("\n7. Listar Muebles con descuento al 50%");
		gestor.listarMueblesDescuento(50);
		//9
		System.out.println("\n8. Eliminar mueble");
		gestor.eliminarMueble("PLATSA");
		//10
		System.out.println("\n9. Listar por Tipo de Mueble");
		gestor.listarMueblesTipo("Armarios");
		//11
		System.out.println("\n10. Listar tras rebajar todo a 50%");
		gestor.rebajas(50);
		gestor.listarMueblesDescuento(50);
		//12
		System.out.println("\n11. Listar Tipos");
		gestor.listarTipos();
		
	}
}
