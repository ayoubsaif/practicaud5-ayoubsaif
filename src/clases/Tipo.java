package clases;
/**
 * @author AyoubSaif
 * 
 */
public class Tipo {
	
	int numero;
	String nombre;
	
	/*
	 * Constructor
	 * */

	public Tipo(int numero, String nombre) {
		super();
		this.numero = numero;
		this.nombre = nombre;
	}

	/*
	 * Getter y Setter de Tipo de Mueble
	 * */
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return numero + "-" + nombre;
	}
}
