package clases;
/**
 * @author AyoubSaif
 * 
 */
import java.util.ArrayList;
import java.util.Iterator;

public class GestorMuebles {

	private ArrayList<Tipo> listaTipos;
	private ArrayList<Mueble> listaMuebles;
	
	public GestorMuebles() {
		listaTipos = new ArrayList<Tipo>();
		listaMuebles = new ArrayList<Mueble>();
	}
	/*
	 * Metodos de la Clase Tipo de Mueble
	 * */
	public void altaTipo(int numero, String nombre) {
		if (!existeTipo(numero)) {
			Tipo nuevoTipo = new Tipo(numero, nombre);
			listaTipos.add(nuevoTipo);
		}else {
			System.out.println("El tipo de mueble ya existe!");
		}
	}
	
	public void listarTipos() {
		for (Tipo responsable : listaTipos) {
			if (responsable != null) {
				System.out.println(responsable);
			}
		}
	}
	
	public Tipo buscarTipo(int numero) {
		for (Tipo tipo : listaTipos) {
			if (tipo != null && tipo.getNumero() == numero) {
				return tipo;
			}
		}
		return null;
	}
	
	public void eliminarTipo(int numero) {
		if (existeTipo(numero)) {
			Iterator<Tipo> iteradorTtipos = listaTipos.iterator();
			
			while (iteradorTtipos.hasNext()) {
				Tipo tipo = iteradorTtipos.next();
				if (tipo.getNumero() == numero ) {
					iteradorTtipos.remove();
				}
			}
		}else {
			System.out.println("�El tipo de mueble no existe!");
		}
	}
	
	public boolean existeTipo(int numero) {
		for (Tipo tipo : listaTipos) {
			if (tipo != null && tipo.getNumero() == numero) {
				return true;
			}
		}
		return false;
	}
	
	
	/*
	 * M�todos de la Clase Mueble
	 * */
	public void altaMueble(int numero, String nombre, double precio, int altura, int anchura, int fondo, int descuento,Tipo tipoMueble) {
		Mueble nuevoMueble = new Mueble(numero, nombre, precio, altura, anchura, fondo, descuento, tipoMueble);
		listaMuebles.add(nuevoMueble);
	}
	
	public void eliminarMueble(String nombre) {
		
		Iterator<Mueble> iteradorMuebles = listaMuebles.iterator();
		
		while (iteradorMuebles.hasNext()) {
			Mueble trabajo = iteradorMuebles.next();
			if (trabajo.getNombre().equals(nombre)) {
				iteradorMuebles.remove();
			}
		}
		
	}
	
	public Mueble buscarMueble(String nombreMueble) {
		for (Mueble mueble : listaMuebles) {
			if (mueble != null && mueble.getNombre().equals(nombreMueble)) {
				return mueble;
			}
		}
		return null;
	}
	
	public void listarMueblesDescuento(int descuento) {
		for (Mueble mueble : listaMuebles) {
			if (mueble.getDescuento() == descuento) {
				System.out.println(mueble);
			}
		}
	}
	
	/*
	 * Metodos relacionados
	 * */
	public void listarMueblesTipo(String nombreTipo) {
		for (Mueble mueble : listaMuebles) {
			if (mueble.getTipoMueble() != null && mueble.getTipoMueble().getNombre().equals(nombreTipo)) {
				System.out.println(mueble);
			}
		}
	}
	
	public void asignarTipo(int numero, String nombreMueble) {
		
		if (buscarTipo(numero) != null && buscarMueble(nombreMueble) != null) {
			Tipo tipo = buscarTipo(numero);
			Mueble mueble = buscarMueble(nombreMueble);
			mueble.setTipoMueble(tipo);
		}
	}
	
	/*
	 * Metodos extra
	 * */
	
	public void rebajas(int descuento) {
		for (Mueble mueble : listaMuebles) {
			if (mueble.getDescuento() == 0) {
				mueble.setDescuento(descuento);
			}
		}
	}
}
