package clases;
/**
 * @author AyoubSaif
 * 
 */
public class Mueble {

	int numero;
	String nombre;
	double precio;
	int altura;
	int anchura;
	int fondo;
	int descuento;
	Tipo TipoMueble;
	
	
	public Mueble(int numero, String nombre, double precio, int altura, int anchura, int fondo, int descuento,
			Tipo tipoMueble) {
		super();
		this.numero = numero;
		this.nombre = nombre;
		this.precio = precio;
		this.altura = altura;
		this.anchura = anchura;
		this.fondo = fondo;
		this.descuento = descuento;
		TipoMueble = tipoMueble;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(int altura) {
		this.altura = altura;
	}
	public double getAnchura() {
		return anchura;
	}
	public void setAnchura(int anchura) {
		this.anchura = anchura;
	}
	public double getFondo() {
		return fondo;
	}
	public void setFondo(int fondo) {
		this.fondo = fondo;
	}
	public int getDescuento() {
		return descuento;
	}
	public void setDescuento(int descuento) {
		this.descuento = descuento;
	}
	public Tipo getTipoMueble() {
		return TipoMueble;
	}
	public void setTipoMueble(Tipo tipoMueble) {
		TipoMueble = tipoMueble;
	}
	
	@Override
	public String toString() {
		return numero + "-" + nombre + "-" + precio + "-" + altura + "-" + anchura + "-" + fondo + "-" + TipoMueble;
	}
	
	
}
